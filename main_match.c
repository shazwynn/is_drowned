/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsauron <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/18 04:28:06 by jsauron           #+#    #+#             */
/*   Updated: 2018/02/18 15:42:46 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

int		match(char *s1, char *s2);

int		main(void)
{
/*	if (argc == 3)
	{
		printf("%s et %s -> %d\n\n", argv[1], argv[2], match(argv[1], argv[2]));	
	}
*/
	char s1[] = "dfidefix";
	char s2[] = "*defix******";
	match(s1, s2);
	printf("%s et %s (ok) : %d\n", s1, s2, match(s1, s2));

	printf("abcdefbcdefdefdeddefidedefidedefiidefffdeeeeddddddffffiiiidfidefix et *******************************defix****** (ok) : %d\n", match("abcdefbcdefdefdeddefidedefidedefiidefffdeeeeddddddffffiiiidfidefix", "*******************************defix******"));
	printf("dfidefix et ****************defix****** (ok) : %d\n", match("dfidefix", "****************defix******"));
	
	printf("sushi et *i**** (ok) : %d\n", match("sushi", "*i****"));
	printf("sushi* et *i (0) : %d\n", match("sushi*", "*i"));
	printf("sushi** et i* (0) : %d\n", match("sushi**", "i*"));
	printf("sushi**** et *i* (ok) : %d\n\n", match("sushi****", "*i*"));

	printf("bonjour et bonjournon (0) : %d\n", match("bonjour", "bonjournon"));
	printf("bonjour et bon*our (ok) : %d\n", match("bonjour", "bon*our"));
	printf("traduction et traduction (ok) : %d\n", match("traduction", "traduction"));
	printf("traduction et traduct (0) : %d\n", match("traduction", "traduc"));
	printf("bonjour et bon*o*u*r (ok) : %d\n", match("bonjour", "bon*o*u*r"));
	printf("bonjour et bon*d (0) : %d\n", match("bonjour", "bon*d"));
	printf("bonjour et bon****r (ok) : %d\n", match("bonjour", "bon****r"));
	printf("bonjour et b* (ok) : %d\n", match("bonjour", "b*"));
	printf("bonjour et bon*jour (ok) : %d\n", match("bonjour", "bon*jour"));
	printf("bonjour et **bon*ur (ok) : %d\n", match("bonjour", "**bon*ur"));
	printf("bonjour et bon*** (ok) : %d\n\n", match("bonjour", "bon***"));
	
	printf("\"\" et \"\" (1) : %d\n", match("", ""));
	printf("\"\" et \"*\" (1) : %d\n", match("", "*"));
	printf("\"*\" et \"\" (0) : %d\n", match("*", ""));
	printf("\"*\" et \"*\" (1) : %d\n", match("*", "*"));
	printf("0QrUW4uhGtI et \"*\" (1) : %d\n", match("0QrUW4uhGtI", "*"));
	printf("\"*\" et v0RqifkG (0) : %d\n", match("*", "v0RqifkG"));
	char *s3;
	char *s4;

	s3 = "gk9FLanJl.c";
	s4 = "*.c";
	printf("%s et %s (1) : %d\n", s3, s4, match(s3, s4));
	s3 = "z2bQGk"; 
	s4 = "z2bQGk";
	printf("%s et %s (1) : %d\n", s3, s4, match(s3, s4));
	s3 = "**c*k";
	s4 = "PxBDkSpQ**z*OeGl*";
	printf("%s et %s (0) : %d\n", s3, s4, match(s3, s4));
	s3 = "Plv";
	s4 = "vHk";
	printf("%s et %s (0) : %d\n", s3, s4, match(s3, s4));
	s3 = "LzjJ*";
	s4 = "I*Av*";
	printf("%s et %s (0) : %d\n", s3, s4, match(s3, s4));
	s3 = "KnNB*AZ*lsQ*LM";
	s4 = "jUzDStsie";
	printf("%s et %s (0) : %d\n", s3, s4, match(s3, s4));
	s3 = "*eOdBa*cEi"; 
	s4 = "GDygSn*PsELXdr";
	printf("%s et %s (0) : %d\n", s3, s4, match(s3, s4));
	s3 = "L*E";
	s4 = "oNyG*q*KOP*BRlfkh";
	printf("%s et %s (0) : %d\n", s3, s4, match(s3, s4));
	s3 = "*G******"; 
	s4 = "*G******";
	printf("%s et %s (1) : %d\n", s3, s4, match(s3, s4));
	s3 = "***X****";
	s4 = "X*******";
	printf("%s et %s (0) : %d\n", s3, s4, match(s3, s4));
	s3 = "****N******";
	s4 = "*******N***";
	printf("%s et %s (1) : %d\n", s3, s4, match(s3, s4));

}
