/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/12 21:25:28 by algrele           #+#    #+#             */
/*   Updated: 2018/02/19 19:56:12 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdlib.h>
#include <string.h>

void	m_putchar(char c)
{
	write(1, &c, 1);
}

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

void	m_putstr(char *str)
{
	int i;

	i = 0;
	if (str)
	{
		while (str[i])
		{
			m_putchar(str[i]);
			i++;
		}
	}
}

int		m_strlen(char *str)
{
	int	i;

	i = 0;
	while (str[i])
		i++;
	return (i);
}

void		m_putnbr(int nb)
{
	int		i;
	char	*smi;

	i = 0;
	smi = "-2147483648";
	if (nb == -2147483648)
		while (i < 11)
			m_putchar(smi[i++]);
	else
	{
		if (nb < 0)
		{
			m_putchar('-');
			nb = -nb;
		}
		if (nb >= 10)
		{
			m_putnbr(nb / 10);
			m_putnbr(nb % 10);
		}
		else
			m_putchar(nb + '0');
	}
}

int		m_atoi(char *str)
{
	int	res;
	int i;
	int s;

	i = 0;
	res = 0;
	s = 0;
	if (str[s] == '\200')
		return (0);
	while (str[s] == ' ' || str[s] <= 31 || str[s] >= 127)
		s++;
	if (str[s] == '+' || str[s] == '-')
		i++;
	while (str[i + s] > 47 && str[i + s] < 58)
	{
		res = res * 10 + str[i + s] - 48;
		i++;
	}
	if (str[s] == '-')
		res = -res;
	return (res);
}

void	ft_putmessage(char *str)
{
	m_putstr("\n\e[38;5;13m");
	m_putstr(str);
	m_putstr("\033[0m\n\n");
}

void	m_pipe(void)
{
	m_putstr("\e[38;5;13m");
	m_putstr(" || ");
	m_putstr("\033[0m");
}

void	b_pipe(void)
{
	m_putstr("\e[38;5;39m");
	m_putstr(" || ");
	m_putstr("\033[0m");
}

void	m_yours(void)
{
	m_putstr("-- le tiens");
	m_pipe();
	m_putstr("le resultat --\n");
}

// prototypes

void	ft_putstr(char *str);
void	ft_putnbr(int nb);
int		ft_atoi(char *str);
char	*ft_strcpy(char *dest, char *src);
char	*ft_strncpy(char *dest, char *src, unsigned int n);
char	*ft_strstr(char *str, char *to_find);
int		ft_strcmp(char *dest, char *src);
int		ft_strncmp(char *dest, char *src, int nb);
char	*ft_strupcase(char *str);
char	*ft_strlowcase(char *str);
char	*ft_strcapitalize(char *str);
int		ft_str_is_alpha(char *str);
int		ft_str_is_numeric(char *str);
int		ft_str_is_lowercase(char *str);
int		ft_str_is_uppercase(char *str);
int		ft_str_is_printable(char *str);
char	*ft_strcat(char *dest, char *src);
char	*ft_strncat(char *dest, char *src, int nb);
unsigned int	ft_strlcat(char *dest, char *src, unsigned int size);
unsigned int	ft_strlcpy(char *dest, char *src, unsigned int size);
void	ft_putnbr_base(int nb, char *base);
int		ft_atoi_base(char *str, char *base);
void	ft_putstr_non_printable(char *str);
//void	*ft_print_memory(void *addr, unsigned int size);

int		main(int argc, char **argv)
{
	if (argc == 1)
	{
		m_putstr("\e[38;5;39m");
		m_putstr("\n0 -> putstr\n");
		m_putstr("1 -> putnbr\n");
		m_putstr("2 -> atoi\n");
		m_putstr("3 -> strcpy\n");
		m_putstr("4 -> strncpy\n");
		m_putstr("5 -> strstr\n");
		m_putstr("6 -> strcmp\n");
		m_putstr("7 -> strncmp\n");
		m_putstr("8 -> strupcase\n");
		m_putstr("9 -> strlowcase\n");
		m_putstr("10 -> strcapitalize\n");
		m_putstr("11 -> str_is_alpha\n");
		m_putstr("12 -> str_is_numeric\n");
		m_putstr("13 -> str_is_lowercase\n");
		m_putstr("14 -> str_is_uppercase\n");
		m_putstr("15 -> str_is_printable\n");
		m_putstr("16 -> strcat\n");
		m_putstr("17 -> strncat\n");
		m_putstr("18 -> strlcat\n");
		m_putstr("19 -> strlcpy\n");
		m_putstr("20 -> putnbr_base\n");
		m_putstr("21 -> atoi_base\n");
		m_putstr("22 -> putstr_non_printable\n");
		m_putstr("23 -> print_memory\n");
		m_putstr("\033[0m\n");
		return (0);
	}

	if (m_atoi(argv[1]) == 0)
	{
		ft_putmessage("ft_putstr");
		ft_putstr("Hello World !!!");
		ft_putstr("");
		ft_putstr("\n");
		return (0);
	}
	else if (m_atoi(argv[1]) == 1)
	{
		int n[13] = {0, 1, -435, 7, 9, 71, 4451, 4452, 98652, 2147483647, -0, -1, -2147483648};
		ft_putmessage("ft_putnbr");
		m_yours();
		int i;
		i = 0;
		while (i < 13)
		{
			ft_putnbr(n[i]);
			m_pipe();
			m_putnbr(n[i]);
			m_putchar('\n');
			i++;
		}
		m_putchar('\n');
		return (0);
	}

	else if (m_atoi(argv[1]) == 2)
	{
		char **tab;
		int i = 0;
		tab = (char **)malloc(400);
		ft_putmessage("ft_atoi");
		m_yours();

		tab[0] = "-02345";
		tab[1] = "2345";
		tab[2] = "00002345";
		tab[3] = "0000-2345";
		tab[4] = "02345";
		tab[5] = "-0hello2345";
		tab[6] = "1234hello";
		tab[7] = "-i1234hello";
		tab[8] = "0000-2345";
		tab[9] = "chraras;ava a'219b1;q56b*= 2698 2q0000-2345";
		tab[10] = "  \e\t    037a574a0-2345";
		tab[11] = "+2147483647";
		tab[12] = "-2147483648";
		tab[13] = "\0";
		tab[14] = "2147483648";
		tab[15] = "++42";
		tab[16] = "4563524613535414748364842";

		while (i < 17)
		{
			m_putnbr(ft_atoi(tab[i]));
			m_pipe();
			m_putnbr(m_atoi(tab[i]));
			m_putchar('\n');
			i++;
		}
		m_putchar('\n');
		free(tab);
		return (0);
	}
	else if (m_atoi(argv[1]) == 3)
	{
		ft_putmessage("ft_strcpy");
		char	dest[] = "0000000";
		char	src[] = "bonjour";
		char	src2[] = "";
		char	src3[] = "he ll l o";
		char	src4[] = "sal\0ut";

		m_putstr(src);
		m_pipe();
		m_putstr(dest);
		m_putchar('\n');
		ft_strcpy(dest, src);
		m_putstr(src);
		m_pipe();
		m_putstr(dest);
		m_putstr("\n\n");

		m_putstr(src2);
		m_pipe();
		m_putstr(dest);
		m_putchar('\n');
		ft_strcpy(dest, src2);
		m_putstr(src2);
		m_pipe();
		m_putstr(dest);
		m_putstr("\n\n");

		m_putstr(src3);
		m_pipe();
		m_putstr(dest);
		m_putchar('\n');
		ft_strcpy(dest, src3);
		m_putstr(src3);
		m_pipe();
		m_putstr(dest);
		m_putstr("\n\n");

		m_putstr(src4);
		m_pipe();
		m_putstr(dest);
		m_putchar('\n');
		ft_strcpy(dest, src4);
		m_putstr(src4);
		m_pipe();
		m_putstr(dest);
		m_putstr("\n\n");
		return (0);
	}
	else if (m_atoi(argv[1]) == 4)
	{
		ft_putmessage("ft_strncpy");
		char	dest[] = "0000000";
		char	src[] = "bonjour";
		char	src2[] = "";
		char	src3[] = "he ll l o";
		char	src4[] = "sal\0ut";

		m_putstr(src);
		m_pipe();
		m_putstr(dest);
		m_pipe();
		m_putnbr(5);
		m_putchar('\n');
		ft_strncpy(dest, src, 5);
		m_putstr(src);
		m_pipe();
		m_putstr(dest);
		m_putstr("\n\n");

		m_putstr(src2);
		m_pipe();
		m_putstr(dest);
		m_pipe();
		m_putnbr(12);
		m_putchar('\n');
		ft_strncpy(dest, src2, 12);
		m_putstr(src2);
		m_pipe();
		m_putstr(dest);
		m_putstr("\n\n");

		m_putstr(src3);
		m_pipe();
		m_putstr(dest);
		m_pipe();
		m_putnbr(0);
		m_putchar('\n');
		ft_strncpy(dest, src3, 0);
		m_putstr(src3);
		m_pipe();
		m_putstr(dest);
		m_putstr("\n\n");

		m_putstr(src4);
		m_pipe();
		m_putstr(dest);
		m_pipe();
		m_putnbr(5);
		m_putchar('\n');
		ft_strncpy(dest, src4, 5);
		m_putstr(src4);
		m_pipe();
		m_putstr(dest);
		m_putstr("\n");
		return (0);
	}
	else if (m_atoi(argv[1]) == 5)
	{
		ft_putmessage("ft_strstr");
		char	haystack[] = "0000000";
		char	haystack2[] = "bonjour";
		char	needle[] = "on";
		char	needle2[] = "";
		//char	src2[] = "";
		//char	src3[] = "he ll l o";
		//char	src4[] = "sal\0ut";

		m_putstr("botte");
		m_pipe();
		m_putstr("aiguille");
		b_pipe();
		m_putstr("le vrai");
		m_pipe();
		m_putstr("le tiens\n\n");

		m_putstr(haystack);
		m_pipe();
		m_putstr(needle);
		b_pipe();
		m_putstr(strstr(haystack, needle));
		m_pipe();
		m_putstr(ft_strstr(haystack, needle));
		m_putstr("\n\n");

		m_putstr(haystack2);
		m_pipe();
		m_putstr(needle);
		b_pipe();
		m_putstr(strstr(haystack2, needle));
		m_pipe();
		m_putstr(ft_strstr(haystack2, needle));
		m_putstr("\n\n");

		m_putstr(haystack2);
		m_pipe();
		m_putstr(needle2);
		b_pipe();
		m_putstr(strstr(haystack2, needle2));
		m_pipe();
		m_putstr(ft_strstr(haystack2, needle2));
		m_putstr("\n\n");

		m_putstr("null");
		m_pipe();
		m_putstr(needle2);
		b_pipe();
		m_putstr(strstr(NULL, needle2));
		m_pipe();
		m_putstr(ft_strstr(NULL, needle2));
		m_putstr("\n\n");

		m_putstr(haystack2);
		m_pipe();
		m_putstr("null");
		b_pipe();
		m_putstr("SEGFAULT");
		m_pipe();
		m_putstr(ft_strstr(haystack2, NULL));
		m_putstr("\n\n");
		return (0);
	}
	else if (m_atoi(argv[1]) == 6)
	{
		ft_putmessage("ft_strcmp");
		char	string1[] = "abcdef";
		char	string2[] = "abcefd";
		char	string3[] = "";
		char	string4[] = "sfkaaj;nbsaf";
		char	string5[] = ".gslhsdgl345k";
		//		char	*string6 = NULL;

		m_putstr("s1");
		m_pipe();
		m_putstr("s2");
		b_pipe();
		m_putstr("le vrai");
		m_pipe();
		m_putstr("le tiens\n\n");

		m_putstr(string1);
		m_pipe();
		m_putstr(string2);
		b_pipe();
		m_putnbr(strcmp(string1, string2));
		m_pipe();
		m_putnbr(ft_strcmp(string1, string2));
		m_putstr("\n\n");

		m_putstr(string1);
		m_pipe();
		m_putstr(string1);
		b_pipe();
		m_putnbr(strcmp(string1, string1));
		m_pipe();
		m_putnbr(ft_strcmp(string1, string1));
		m_putstr("\n\n");

		m_putstr("\"\"");
		m_pipe();
		m_putstr(string4);
		b_pipe();
		m_putnbr(strcmp(string3, string4));
		m_pipe();
		m_putnbr(ft_strcmp(string3, string4));
		m_putstr("\n\n");

		m_putstr(string4);
		m_pipe();
		m_putstr("\"\"");
		b_pipe();
		m_putnbr(strcmp(string4, string3));
		m_pipe();
		m_putnbr(ft_strcmp(string4, string3));
		m_putstr("\n\n");

		m_putstr(string4);
		m_pipe();
		m_putstr(string5);
		b_pipe();
		m_putnbr(strcmp(string4, string5));
		m_pipe();
		m_putnbr(ft_strcmp(string4, string5));
		m_putstr("\n\n");

		/*		m_putstr(string5);
				m_pipe();
				m_putstr(string6);
				b_pipe();
				ft_putstr("SEGFAULT");
				m_putnbr(strcmp(string5, string6));
				m_pipe();
				m_putnbr(ft_strcmp(string5, string6));
				m_putstr("\n\n");
				*/
		return (0);

	}
	else if (m_atoi(argv[1]) == 7)
	{
		ft_putmessage("ft_strncmp");
		char	string1[] = "helo";
		char	string2[] = "hello";
		char	string3[] = "abcdef";
		char	string4[] = "sfkaaj;nbsaf";
		char	string5[] = ".gslhsdgl345k";
		//		char	*string6 = NULL;

		m_putstr("s1");
		m_pipe();
		m_putstr("s2");
		b_pipe();
		m_putstr("n");
		b_pipe();
		m_putstr("le vrai");
		m_pipe();
		m_putstr("le tiens\n\n");

		m_putstr(string1);
		m_pipe();
		m_putstr(string2);
		b_pipe();
		m_putnbr(5);
		b_pipe();
		m_putnbr(strncmp(string1, string2, 5));
		m_pipe();
		m_putnbr(ft_strncmp(string1, string2, 5));
		m_putstr("\n\n");

		m_putstr(string1);
		m_pipe();
		m_putstr(string1);
		b_pipe();
		m_putnbr(8);
		b_pipe();
		m_putnbr(strncmp(string1, string1, 8));
		m_pipe();
		m_putnbr(ft_strncmp(string1, string1, 8));
		m_putstr("\n\n");

		m_putstr("\"\"");
		m_pipe();
		m_putstr(string4);
		b_pipe();
		m_putnbr(2);
		b_pipe();
		m_putnbr(strncmp(string3, string4, 2));
		m_pipe();
		m_putnbr(ft_strncmp(string3, string4, 2));
		m_putstr("\n\n");

		m_putstr(string4);
		m_pipe();
		m_putstr("\"\"");
		b_pipe();
		m_putnbr(0);
		b_pipe();
		m_putnbr(strncmp(string4, string3, 0));
		m_pipe();
		m_putnbr(ft_strncmp(string4, string3, 0));
		m_putstr("\n\n");

		m_putstr(string4);
		m_pipe();
		m_putstr(string5);
		b_pipe();
		m_putnbr(-1);
		b_pipe();
		m_putnbr(strncmp(string4, string5, -1));
		m_pipe();
		m_putnbr(ft_strncmp(string4, string5, -1));
		m_putstr("\n\n");

		/*		m_putstr(string5);
				m_pipe();
				m_putstr(string6);
				b_pipe();
				ft_putstr("SEGFAULT");
				m_putnbr(strcmp(string5, string6));
				m_pipe();
				m_putnbr(ft_strcmp(string5, string6));
				m_putstr("\n\n");
				*/

		return (0);
	}
	else if (m_atoi(argv[1]) == 8)
	{
		ft_putmessage("ft_strupcase");
		char s1[] = "bonjour";
		char s2[] = "BOnjOuuuuuuuR";
		char s3[] = "sarhvashl 153 05 4 87 45awpo23945-.,z;q'qg";
		char s4[] = "";
		m_putstr(s1);
		m_pipe();
		m_putstr(ft_strupcase(s1));
		m_putchar('\n');

		m_putstr(s2);
		m_pipe();
		m_putstr(ft_strupcase(s2));
		m_putchar('\n');

		m_putstr(s3);
		m_pipe();
		m_putstr(ft_strupcase(s3));
		m_putchar('\n');

		m_putstr("\"\"");
		m_pipe();
		m_putstr(ft_strupcase(s4));
		m_putchar('\n');
		return (0);

	}
	else if (m_atoi(argv[1]) == 9)
	{
		ft_putmessage("ft_strlowcase");
		char s1[] = "BONJOUR";
		char s2[] = "BOnjOuuuuuuuR";
		char s3[] = "sarhRRRCCcashlW 15T3 0W5 4 x87 45awpo23945-.,z;q'qg";
		char s4[] = "";
		m_putstr(s1);
		m_pipe();
		m_putstr(ft_strlowcase(s1));
		m_putchar('\n');

		m_putstr(s2);
		m_pipe();
		m_putstr(ft_strlowcase(s2));
		m_putchar('\n');

		m_putstr(s3);
		m_pipe();
		m_putstr(ft_strlowcase(s3));
		m_putchar('\n');

		m_putstr("\"\"");
		m_pipe();
		m_putstr(ft_strlowcase(s4));
		m_putchar('\n');
		return (0);
	}
	else if (m_atoi(argv[1]) == 10)
	{
		ft_putmessage("ft_strcapitalize");
		char str[] = "whAt,     HaPPEns/TO a. \\dream 42deferred doe_s it Fester l.<.??.,ikeA raiSIN in the S0n";
		char resultat[] = "What,     Happens/To A. \\Dream 42deferred Doe_S It Fester L.<.??.,Ikea Raisin In The S0n";
		char vide[] = ""; 

		m_putstr("originale : ");
		m_putstr(str);
		m_putchar('\n');
		m_putstr("\e[38;5;39m");
		m_putstr("resultat  : ");
		m_putstr(resultat);
		m_putstr("\033[0m");
		m_putchar('\n');
		m_putstr("la tienne : ");
		m_putstr(ft_strcapitalize(str));
		m_putchar('\n');

		m_putstr("originale : ");
		m_putstr("\"\"");
		m_putchar('\n');
		m_putstr("\e[38;5;39m");
		m_putstr("resultat  : ");
		m_putstr("\033[0m");
		m_putchar('\n');
		m_putstr("la tienne : ");
		m_putstr(ft_strcapitalize(vide));
		m_putchar('\n');
		return (0);

	}
	else if (m_atoi(argv[1]) == 11)
	{
		ft_putmessage("str_is_alpha");
		char s1[] = "absncasnlejfsfabljebfleajff";
		char s2[] = "abcdwa0";
		char s3[] = "";
		char s4[] = "asd asd asdlgha eangljbgae" ;

		m_putstr(s1);
		m_pipe();
		m_putstr("attendu " );
		m_putnbr(1);
		m_pipe();
		m_putnbr(ft_str_is_alpha(s1));
		m_putchar('\n');

		m_putstr(s2);
		m_pipe();
		m_putstr("attendu " );
		m_putnbr(0);
		m_pipe();
		m_putnbr(ft_str_is_alpha(s2));
		m_putchar('\n');

		m_putstr(s4);
		m_pipe();
		m_putstr("attendu " );
		m_putnbr(0);
		m_pipe();
		m_putnbr(ft_str_is_alpha(s4));
		m_putchar('\n');

		m_putstr("\"\"");
		m_pipe();
		m_putstr("attendu " );
		m_putnbr(1);
		m_pipe();
		m_putnbr(ft_str_is_alpha(s3));
		m_putchar('\n');
		return (0);

	}
	else if (m_atoi(argv[1]) == 12)
	{
		ft_putmessage("str_is_numeric");
		char s1[] = "013468473132135435351312987546231205";
		char s2[] = "1644121542a0";
		char s3[] = "";
		char s4[] = "0413 13510 3454 1" ;

		m_putstr(s1);
		m_pipe();
		m_putstr("attendu " );
		m_putnbr(1);
		m_pipe();
		m_putnbr(ft_str_is_numeric(s1));
		m_putchar('\n');

		m_putstr(s2);
		m_pipe();
		m_putstr("attendu " );
		m_putnbr(0);
		m_pipe();
		m_putnbr(ft_str_is_numeric(s2));
		m_putchar('\n');

		m_putstr(s4);
		m_pipe();
		m_putstr("attendu " );
		m_putnbr(0);
		m_pipe();
		m_putnbr(ft_str_is_numeric(s4));
		m_putchar('\n');

		m_putstr("\"\"");
		m_pipe();
		m_putstr("attendu " );
		m_putnbr(1);
		m_pipe();
		m_putnbr(ft_str_is_numeric(s3));
		m_putchar('\n');
		return (0);

	}
	else if (m_atoi(argv[1]) == 13)
	{
		ft_putmessage("str_is_lowercase");
		char s1[] = "abcdemlokeproiqlwoeireuwutytfgsdsadkglasjkdgmnvbxcnz";
		char s2[] = "adsnkadhklashA";
		char s3[] = "";
		char s4[] = "a" ;

		m_putstr(s1);
		m_pipe();
		m_putstr("attendu " );
		m_putnbr(1);
		m_pipe();
		m_putnbr(ft_str_is_lowercase(s1));
		m_putchar('\n');

		m_putstr(s2);
		m_pipe();
		m_putstr("attendu " );
		m_putnbr(0);
		m_pipe();
		m_putnbr(ft_str_is_lowercase(s2));
		m_putchar('\n');

		m_putstr(s4);
		m_pipe();
		m_putstr("attendu " );
		m_putnbr(1);
		m_pipe();
		m_putnbr(ft_str_is_lowercase(s4));
		m_putchar('\n');

		m_putstr("\"\"");
		m_pipe();
		m_putstr("attendu " );
		m_putnbr(1);
		m_pipe();
		m_putnbr(ft_str_is_lowercase(s3));
		m_putchar('\n');
		return (0);

	}
	else if (m_atoi(argv[1]) == 14)
	{
		ft_putmessage("str_is_uppercase");
		char s1[] = "DLKHSGKHQWLPOIUYTHRGFDESWAXZCCVBBNMALDK";
		char s2[] = "SD LDKFSJGLKSJG";
		char s3[] = "";
		char s4[] = "Q" ;

		m_putstr(s1);
		m_pipe();
		m_putstr("attendu " );
		m_putnbr(1);
		m_pipe();
		m_putnbr(ft_str_is_uppercase(s1));
		m_putchar('\n');

		m_putstr(s2);
		m_pipe();
		m_putstr("attendu " );
		m_putnbr(0);
		m_pipe();
		m_putnbr(ft_str_is_uppercase(s2));
		m_putchar('\n');

		m_putstr(s4);
		m_pipe();
		m_putstr("attendu " );
		m_putnbr(1);
		m_pipe();
		m_putnbr(ft_str_is_uppercase(s4));
		m_putchar('\n');

		m_putstr("\"\"");
		m_pipe();
		m_putstr("attendu " );
		m_putnbr(1);
		m_pipe();
		m_putnbr(ft_str_is_uppercase(s3));
		m_putchar('\n');
		return (0);

	}
	else if (m_atoi(argv[1]) == 15)
	{
		ft_putmessage("str_is_printable");
		char s1[] = "lhljghlhflih\eK";
		char s2[] = "SD LDKFSJGLK02saz./,'.[;]-0988887655431#$%^!2SJG";
		char s3[] = "";

		m_putstr(s1);
		m_pipe();
		m_putstr("attendu " );
		m_putnbr(0);
		m_pipe();
		m_putnbr(ft_str_is_printable(s1));
		m_putchar('\n');

		m_putstr(s2);
		m_pipe();
		m_putstr("attendu " );
		m_putnbr(1);
		m_pipe();
		m_putnbr(ft_str_is_printable(s2));
		m_putchar('\n');

		m_putstr("\"\"");
		m_pipe();
		m_putstr("attendu " );
		m_putnbr(1);
		m_pipe();
		m_putnbr(ft_str_is_printable(s3));
		m_putchar('\n');
		return (0);

	}
	else if (m_atoi(argv[1]) == 16)
	{
		ft_putmessage("ft_strcat");
		char src[] = "jour";
		char dest[8];
		dest[0] = 'b';
		dest[1] = 'o';
		dest[2] = 'n';
		dest[3] = '\0';
		dest[4] = ' ';
		dest[5] = ' ';
		dest[6] = ' ';

		m_putstr("source : ");
		m_putstr(src);
		m_putstr("\ndestination : ");
		m_putstr(dest);
		m_putchar('\n');
		m_putnbr(m_strlen(dest));
		m_putchar('\n');
		ft_strcat(dest, src);
		m_putstr("source : ");
		m_putstr(src);
		m_putchar('\n');
		m_putstr("destination : ");
		m_putstr(dest);
		return (0);
	}
	else if (m_atoi(argv[1]) == 17)
	{
		ft_putmessage("ft_strncat");
		char src[] = "jour";
		char dest[8];
		int nb = 3;

		dest[0] = 'b';
		dest[1] = 'o';
		dest[2] = 'n';
		dest[3] = '\0';
		dest[4] = ' ';
		dest[5] = ' ';
		dest[6] = ' ';

		m_putstr("source : ");
		m_putstr(src);
		m_putstr("\ndestination : ");
		m_putstr(dest);
		m_putchar('\n');
		m_putnbr(m_strlen(dest));
		m_putchar('\n');
		ft_strncat(dest, src, nb);
		m_putstr("source : ");
		m_putstr(src);
		m_putchar('\n');
		m_putstr("destination : ");
		m_putstr(dest);
		return (0);
	}
	else if (m_atoi(argv[1]) == 18)
	{
		ft_putmessage("ft_strlcat");
		char src[] = "jour";
		char dest[8];
		int nb = 6;

		dest[0] = 'b';
		dest[1] = 'o';
		dest[2] = 'n';
		dest[3] = '\0';
		dest[4] = ' ';
		dest[5] = ' ';
		dest[6] = ' ';

		m_putstr("source : ");
		m_putstr(src);
		m_putstr("\ndestination : ");
		m_putstr(dest);
		m_putchar('\n');
		m_putstr("\e[38;5;39m");
		m_putstr(" strlcat --> ");
		ft_putnbr(ft_strlcat(dest, src, nb));
		m_putstr("\033[0m");
		m_putchar('\n');
		m_putstr("source : ");
		m_putstr(src);
		m_putchar('\n');
		m_putstr("destination : ");
		m_putstr(dest);
		m_putchar('\n');
		m_putchar('\n');

		dest[0] = 'b';
		dest[1] = 'o';
		dest[2] = 'n';
		dest[3] = '\0';
		dest[4] = ' ';
		dest[5] = ' ';
		dest[6] = ' ';

		m_putstr("source : ");
		m_putstr(src);
		m_putstr("\ndestination : ");
		m_putstr(dest);
		m_putchar('\n');
		m_putstr("\e[38;5;39m");
		m_putstr(" strlcat --> ");
		ft_putnbr(strlcat(dest, src, nb));
		m_putstr("\033[0m");
		m_putchar('\n');
		m_putstr("source : ");
		m_putstr(src);
		m_putchar('\n');
		m_putstr("destination : ");
		m_putstr(dest);

		return (0);
	}
	else if (m_atoi(argv[1]) == 19)
	{
		ft_putmessage("ft_strlcpy");
		char	src[] = "bonj";
		char	dest[] = "1111111";
		char	dest2[] = "1111111";
		int 	n = 7;

		m_putstr("source : ");
		m_putstr(src);
		m_putstr("\ndestination : ");
		m_putstr(dest);
		m_putchar('\n');
		m_putstr("\e[38;5;39m");
		m_putstr("ft_strlcpy --> ");
		m_putnbr(ft_strlcpy(dest, src, n));
		m_putstr("\033[0m");
		m_putchar('\n');
		m_putstr("source : ");
		m_putstr(src);
		m_putchar('\n');
		m_putstr("destination : ");
		m_putstr(dest);
		m_putchar('\n');

		m_putchar('\n');
		m_putstr("source : ");
		m_putstr(src);
		m_putstr("\ndestination : ");
		m_putstr(dest2);
		m_putchar('\n');
		m_putstr("\e[38;5;39m");
		m_putstr("strlcpy --> ");
		m_putnbr(strlcpy(dest2, src, n));
		m_putstr("\033[0m");
		m_putchar('\n');
		m_putstr("source : ");
		m_putstr(src);
		m_putchar('\n');
		m_putstr("destination : ");
		m_putstr(dest2);
		return (0);
	}
	else if (m_atoi(argv[1]) == 20)
	{
		ft_putmessage("ft_putnbr_base");
		ft_putstr("le tiens  ");
		ft_putnbr_base(-2147483648, "bonjur");
		m_pipe();
		ft_putnbr_base(-232, "02");
		m_pipe();
		ft_putnbr_base(12345678, "0123456789");
		m_pipe();
		ft_putnbr_base(231, "BONJUR");
		m_pipe();
		ft_putnbr_base(2147483647, "hiya");
		m_putchar('\n');
		ft_putstr("le mien   ");
		m_putstr("-rrjbjnbbrrjn");
		b_pipe();
		m_putstr("-22202000");
		b_pipe();
		ft_putstr("12345678");
		b_pipe();
		m_putstr("OBNJ");
		b_pipe();
		m_putstr("iaaaaaaaaaaaaaaa");
		return (0);
	}
	else if (m_atoi(argv[1]) == 21)
	{
		ft_putmessage("ft_atoi_base");
		int res = 0;
		char str[] = "   8bf4dgsthlath==04u86";
		char base[] = "0123456789abcdef";
		char str2[] = "-def";
		char base2[] = "";
		m_putnbr(ft_atoi_base(str, base));
		m_putchar('\n');
		m_putnbr(ft_atoi_base(str2, base2));
		m_putchar('\n');
		char str3[] = "231";
		char base3[] = "0123456";
		char str4[] = "NJO";
		char base4[] = "BONJURI";
		res = ft_atoi_base(str3, base3);
		m_putnbr(res);
		m_putchar('\n');
		res = ft_atoi_base(str4, base4);
		m_putnbr(res);
		m_putchar('\n');
		res = ft_atoi_base("10000", "01");
		m_putnbr(res);
		m_putchar('\n');
		res = ft_atoi_base("2-a", "0123456789ABCDEF");
		m_putnbr(res);
		m_putchar('\n');
		res = ft_atoi_base("BDACA", "ABCD");
		m_putnbr(res);
		m_putchar('\n');
		return (0);
	}
	else if (m_atoi(argv[1]) == 22)
	{
		ft_putmessage("ft_putstr_non_printable");
		ft_putstr_non_printable("This is\e my \tmessage");
		m_putchar('\n');
		char str[300];
		ft_putstr_non_printable("Coucou\ntu \evas bien ?");
		m_putchar('\n');

		str[0] = 1;
		str[1] = 1;
		str[2] = 2;
		str[3] = 3;
		str[4] = 4;
		str[5] = 5;
		str[6] = 6;
		str[7] = 7;
		str[8] = 8;
		str[9] = 9;
		str[10] = 10;
		str[11] = 11;
		str[12] = 12;
		str[13] = 13;
		str[14] = 14;
		str[15] = 15;
		str[16] = 16;
		str[17] = 17;
		str[18] = 18;
		str[19] = 19;
		str[20] = 20;
		str[21] = 21;
		str[22] = 22;
		str[23] = 23;
		str[24] = 24;
		str[25] = 25;
		str[26] = 26;
		str[27] = 27;
		str[28] = 28;
		str[29] = 29;
		str[30] = 30;
		str[31] = 31;
		str[32] = 32;
		str[33] = 33;
		str[34] = 34;
		str[35] = 35;
		str[36] = 36;
		str[37] = '\0';
		ft_putstr_non_printable(str);
		return (0);
	}
	else if (m_atoi(argv[1]) == 23)
	{
		ft_putmessage("print_memory");
		return (0);
	}
	return (0);
}
//		int n[13] = {0, 1, 2, 7, 9, 71, 4451, 4452, 98652, 2147483647, -0, -1, -2147483648};
