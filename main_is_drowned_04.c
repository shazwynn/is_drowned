/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/12 21:25:28 by algrele           #+#    #+#             */
/*   Updated: 2018/02/13 04:20:11 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

// pour les eight_queens_2 

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

void	m_putchar(char c)
{
	write(1, &c, 1);
}

void	m_putstr(char *str)
{
	int i;

	i = 0;
	if (str)
	{
		while (str[i])
		{
			m_putchar(str[i]);
			i++;
		}
	}
}

void		m_putnbr(int nb)
{
	int		i;
	char	*smi;

	i = 0;
	smi = "-2147483648";
	if (nb == -2147483648)
		while (i < 11)
			m_putchar(smi[i++]);
	else
	{
		if (nb < 0)
		{
			m_putchar('-');
			nb = -nb;
		}
		if (nb >= 10)
		{
			m_putnbr(nb / 10);
			m_putnbr(nb % 10);
		}
		else
			m_putchar(nb + '0');
	}
}

int		m_atoi(char *str)
{
	int	res;
	int i;
	int s;

	i = 0;
	res = 0;
	s = 0;
	if (str[s] == '\200')
		return (0);
	while (str[s] == ' ' || str[s] <= 31 || str[s] >= 127)
		s++;
	if (str[s] == '+' || str[s] == '-')
		i++;
	while (str[i + s] > 47 && str[i + s] < 58)
	{
		res = res * 10 + str[i + s] - 48;
		i++;
	}
	if (str[s] == '-')
		res = -res;
	return (res);
}

int		ft_iterative_factorial(int nb);
int		ft_recursive_factorial(int nb);
int		ft_iterative_power(int nb, int power);
int		ft_recursive_power(int nb, int power);
int		ft_fibonacci(int index);
int		ft_sqrt(int nb);
int		ft_is_prime(int nb);
int		ft_find_next_prime(int nb);
int		ft_eight_queens_puzzle(void);
int		ft_eight_queens_puzzle_2(void);

int		main(int argc, char **argv)
{
	if (argc < 2)
		return (0);

	if (m_atoi(argv[1]) == 0)
	{
		m_putstr("\n\e[38;5;13mft_iterative_factorial\033[0m\n\n");
		m_putnbr(-4);
		m_putstr("! = ");
		m_putnbr(ft_iterative_factorial(-4));
		m_putchar('\n');
		m_putnbr(-0);
		m_putstr("! = ");
		m_putnbr(ft_iterative_factorial(-0));
		m_putchar('\n');
		int i = 0;
		while (i < 14)
		{
			m_putnbr(i);
			m_putstr("! = ");
			m_putnbr(ft_iterative_factorial(i));
			m_putchar('\n');
			i++;
		}
	}
	else if (m_atoi(argv[1]) == 1)
	{
		m_putstr("\n\e[38;5;13mft_recursive_factorial\033[0m\n\n");
		m_putnbr(-4);
		m_putstr("! = ");
		m_putnbr(ft_recursive_factorial(-4));
		m_putchar('\n');
		m_putnbr(-0);
		m_putstr("! = ");
		m_putnbr(ft_recursive_factorial(-0));
		m_putchar('\n');
		int i = 0;
		while (i < 14)
		{
			m_putnbr(i);
			m_putstr("! = ");
			m_putnbr(ft_recursive_factorial(i));
			m_putchar('\n');
			i++;
		}
	}

	else if (m_atoi(argv[1]) == 2)
	{
		m_putstr("\n\e[38;5;13miterative_power -> nb^power\033[0m\n\n");
		if (argc == 4)
			m_putnbr(ft_iterative_power(m_atoi(argv[2]), m_atoi(argv[3])));
		else
			m_putstr("./a.out 2 nb power");
		m_putchar('\n');
	}
	else if (m_atoi(argv[1]) == 3)
	{
		m_putstr("\n\e[38;5;13mrecursive_power\033[0m\n\n");
		if (argc == 4)
			m_putnbr(ft_recursive_power(m_atoi(argv[2]), m_atoi(argv[3])));
		else
			m_putstr("./a.out 3 nb power");
		m_putchar('\n');
	}
	else if (m_atoi(argv[1]) == 4)
	{
		m_putstr("\n\e[38;5;13mfibonacci\033[0m\n\n");
		if (argc == 3)
			m_putnbr(ft_fibonacci(m_atoi(argv[2])));
		else
			m_putstr("./a.out 4 number");
		m_putchar('\n');
	}
	else if (m_atoi(argv[1]) == 5)
	{
		m_putstr("\n\e[38;5;13msqrt\033[0m\n\n");
		if (argc == 3)
			m_putnbr(ft_sqrt(m_atoi(argv[2])));
		else
			m_putstr("./a.out 5 number");
		m_putchar('\n');
	}
	else if (m_atoi(argv[1]) == 6)
	{
		m_putstr("\n\e[38;5;13mis_prime\033[0m\n\n");
		int n[13] = {0, 1, 2, 7, 9, 71, 4451, 4452, 98652, 2147483647, -0, -1, -2147483648};
		int i = 0;

		while (i < 13)
		{
			m_putnbr(n[i]);
			m_putstr(" -> ");
			m_putnbr(ft_is_prime(n[i]));
			m_putchar('\n');
			i++;
		}
	}
	else if (m_atoi(argv[1]) == 7)
	{
		m_putstr("\n\e[38;5;13mfind_next_prime\033[0m\n\n");
		int n[13] = {0, 1, 2, 7, 9, 71, 4451, 4452, 98652, 2147483647, -0, -1, -2147483648};
		int i = 0;

		while (i < 13)
		{
			m_putnbr(n[i]);
			m_putstr(" -> ");
			m_putnbr(ft_find_next_prime(n[i]));
			m_putchar('\n');
			i++;
		}
	}
	else if (m_atoi(argv[1]) == 8)
	{
		m_putstr("\n\e[38;5;13meight_queens_puzzle\033[0m\n\n");
		m_putnbr(ft_eight_queens_puzzle());
		m_putchar('\n');
	}
	else if (m_atoi(argv[1]) == 9)
	{
		m_putstr("\n\e[38;5;13meight_queens_puzzle_2\033[0m\n\n");
		ft_eight_queens_puzzle_2();
	}	
	return (0);
}
