/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsauron <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/18 04:28:06 by jsauron           #+#    #+#             */
/*   Updated: 2018/02/18 17:58:16 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

int		nmatch(char *s1, char *s2);

int		main(void)
{
	char s1[] = "dfidefix";
	char s2[] = "*defix******";
	printf("bonjour et bon*our (ok) : %d\n", nmatch("bonjour", "bon*our"));
	printf("sushi et *i**** (ok) : %d\n", nmatch("sushi", "*i****"));
	printf("%s et %s (ok) : %d\n", s1, s2, nmatch(s1, s2));

	printf("dfidefix et ****************defix****** (ok) : %d\n", nmatch("dfidefix", "****************defix******"));
	
	printf("sushi et *i**** (ok) : %d\n", nmatch("sushi", "*i****"));
	printf("sushi* et *i (0) : %d\n", nmatch("sushi*", "*i"));
	printf("sushi** et i* (0) : %d\n", nmatch("sushi**", "i*"));
	printf("sushi**** et *i* (ok) : %d\n\n", nmatch("sushi****", "*i*"));

	printf("bonjour et bonjournon (0) : %d\n", nmatch("bonjour", "bonjournon"));
	printf("bonjour et bon*our (ok) : %d\n", nmatch("bonjour", "bon*our"));
	printf("traduction et traduction (ok) : %d\n", nmatch("traduction", "traduction"));
	printf("traduction et traduct (0) : %d\n", nmatch("traduction", "traduc"));
	printf("bonjour et bon*o*u*r (ok) : %d\n", nmatch("bonjour", "bon*o*u*r"));
	printf("bonjour et bon*d (0) : %d\n", nmatch("bonjour", "bon*d"));
	printf("bonjour et bon****r (ok) : %d\n", nmatch("bonjour", "bon****r"));
	printf("bonjour et b* (ok) : %d\n", nmatch("bonjour", "b*"));
	printf("bonjour et bon*jour (ok) : %d\n", nmatch("bonjour", "bon*jour"));
	printf("bonjour et **bon*ur (ok) : %d\n", nmatch("bonjour", "**bon*ur"));
	printf("bonjour et bon*** (ok) : %d\n\n", nmatch("bonjour", "bon***"));
	
	printf("\"\" et \"\" (1) : %d\n", nmatch("", ""));
	printf("\"\" et \"*\" (1) : %d\n", nmatch("", "*"));
	printf("\"*\" et \"\" (0) : %d\n", nmatch("*", ""));
	printf("\"*\" et \"*\" (1) : %d\n", nmatch("*", "*"));
	printf("0QrUW4uhGtI et \"*\" (1) : %d\n", nmatch("0QrUW4uhGtI", "*"));
	printf("\"*\" et v0RqifkG (0) : %d\n", nmatch("*", "v0RqifkG"));
	char *s3;
	char *s4;

	s3 = "gk9FLanJl.c";
	s4 = "*.c";
	printf("%s et %s (1) : %d\n", s3, s4, nmatch(s3, s4));
	s3 = "z2bQGk"; 
	s4 = "z2bQGk";
	printf("%s et %s (1) : %d\n", s3, s4, nmatch(s3, s4));
	s3 = "**c*k";
	s4 = "PxBDkSpQ**z*OeGl*";
	printf("%s et %s (0) : %d\n", s3, s4, nmatch(s3, s4));
	s3 = "Plv";
	s4 = "vHk";
	printf("%s et %s (0) : %d\n", s3, s4, nmatch(s3, s4));
	s3 = "LzjJ*";
	s4 = "I*Av*";
	printf("%s et %s (0) : %d\n", s3, s4, nmatch(s3, s4));
	s3 = "KnNB*AZ*lsQ*LM";
	s4 = "jUzDStsie";
	printf("%s et %s (0) : %d\n", s3, s4, nmatch(s3, s4));
	s3 = "*eOdBa*cEi"; 
	s4 = "GDygSn*PsELXdr";
	printf("%s et %s (0) : %d\n", s3, s4, nmatch(s3, s4));
	s3 = "L*E";
	s4 = "oNyG*q*KOP*BRlfkh";
	printf("%s et %s (0) : %d\n", s3, s4, nmatch(s3, s4));
	s3 = "*G******"; 
	s4 = "*G******";
	printf("%s et %s (1) : %d\n", s3, s4, nmatch(s3, s4));
	s3 = "***X****";
	s4 = "X*******";
	printf("%s et %s (0) : %d\n", s3, s4, nmatch(s3, s4));
	s3 = "****N******";
	s4 = "*******N***";
	printf("%s et %s (1) : %d\n", s3, s4, nmatch(s3, s4));
	printf("defdedefidefix et ************defix****** (ok) : %d\n", nmatch("defdedefidefix", "************defix******"));

}
